import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularCesiumModule } from 'angular-cesium';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SemanticComponent } from './semantic/semantic.component';
import { CesiumComponent } from './cesium/cesium.component';
import { SuiModule } from 'ng2-semantic-ui';
import { PointLayerComponent } from "./cesium/pointLayer/point-layer.component";
import { BillboardLayerComponent } from "./cesium/billboardLayer/billboard-layer.component";
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { CounterActions } from './redux/actions/countAction';
import { rootReducer, IAppState, INITIAL_STATE } from './redux/store'; // < New

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SemanticComponent,
    CesiumComponent,
    PointLayerComponent,
    BillboardLayerComponent
  ],
  imports: [
    BrowserModule,
    AngularCesiumModule,
    SuiModule,
    NgReduxModule,
  ],
  providers: [CounterActions],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    // Tell @angular-redux/store about our rootReducer and our initial state.
    // It will use this to create a redux store for us and wire up all the
    // events.
    ngRedux.configureStore(
      rootReducer,
      INITIAL_STATE);
  }
 }

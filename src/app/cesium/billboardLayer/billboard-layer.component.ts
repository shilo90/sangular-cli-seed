import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AcNotification, ActionType } from 'angular-cesium';

@Component({
  moduleId: module.id,
  selector: 'billboard-layer',
  templateUrl: 'billboard-layer.component.html'
})
export class BillboardLayerComponent implements OnInit {

  billboards$: Observable<AcNotification> = new Observable<AcNotification>();
  Cesium = Cesium;
  show = true;

  constructor() {
    const billboardArray = [];
    for (let i = 0; i < 20; i++) {
      let randCenter = Cesium.Cartesian3.fromDegrees(Math.random() * 90 - 40, Math.random() * 90 - 40);
      billboardArray.push({
        id: i,
        actionType: ActionType.ADD_UPDATE,
        entity: {
          position: randCenter,
          image: '/assets/images/F16-Fighting-Falcon-48px.png'
        }
      })
    }
    this.billboards$ = Observable.create(function (observable) {
      billboardArray.forEach(function (billboard) {
        observable.next(billboard);
      });
    });
  }

  ngOnInit() {

  }

}
import { Component, OnDestroy } from '@angular/core';
import { NgRedux } from '@angular-redux/store'; // <- New
import { CounterActions } from '../redux/actions/countAction'; // <- New
import { IAppState } from '../redux/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnDestroy {

  title = 'app';
  count: number;
  subscription;
  
  constructor(private ngRedux: NgRedux<IAppState>,
    private actions: CounterActions) {
      this.subscription = ngRedux.select<number>('count')
      .subscribe(newCount => this.count = newCount);
    }
  increment() {
    this.ngRedux.dispatch(this.actions.increment());
  }
  decrement() {
    this.ngRedux.dispatch(this.actions.decrement());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
